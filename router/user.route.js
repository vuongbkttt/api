var express = require('express');
var multer  = require('multer')
var upload = multer({ dest: './public/uploads/' })
var controller = require('../controllers/user.controller')
var validate = require('../validate/user.validate')
var router = express.Router()
var authMiddleware = require('../middlewares/auth.middleware');



router.get('/', controller.index);

router.get('/create', controller.create);

router.post('/create', upload.single('avatar'), validate.postcreate, controller.postcreate);

router.get('/search', controller.search);
//var matcheUsers2 = users.filter(x => x.username.indexOf(q) !== -1);



router.get('/:id', controller.get);

router.get('/cookie', function(req, res, next){
req.cookies('user-id', '12345');
res.send('Hello');
});

module.exports = router;
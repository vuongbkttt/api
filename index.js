require('dotenv').config();


const express = require('express')
const bodyParser = require('body-parser')

const userRoute = require('./router/user.route')
const productRoute = require('./router/product.route');
var cookieParser = require('cookie-parser')
const authRoute = require('./router/auth.route');

var mongoees = require('mongoose');

var apiProductRoute = require('./api/router/product.router')
// var apiUserRoute = require('./api/router/user.router')
// mongoees.connect(process.env.MONGO_URL);

mongoees.connect('mongodb://localhost/express-demo')

var authMiddleware = require('./middlewares/auth.middleware')

const port = 3000;

const app = express();
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
app.use('/api/products', apiProductRoute)
// app.use('/api/users', apiUserRoute)

app.set('view engine', 'pug')
app.set('views', './views')

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.use(express.static('public'))

app.use(cookieParser(process.env.SESSION_SECRET))


app.get('/', function(req, res){
    res.render('index', {
    name: 'AAA'
    });
});

app.use('/users', userRoute);
app.use('/auth', authRoute)
app.use('/products', productRoute);
// app.use('/users', authMiddleware.requireAuth, userRoute);


app.listen(port, function(){
console.log('Server listenning port'+ port);
});
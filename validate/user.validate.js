module.exports.postcreate = function(req, res, next){
    var errors = [];
    if(!req.body.name){
        errors.push("Name is requie")
    }
    if(!req.body.phone){
        errors.push("Phone is requie")
    }
    if(errors.length){
        res.render('users/create',{
        errors: errors,
        values: req.body
        });
        return;
    }
    res.locals.success = true;
    next();
}
var mongoees = require('mongoose');

var userSchema = new mongoees.Schema({
    name: String,
    phone: String,
    email: String,
    password: String,
});

var User = mongoees.model('User', userSchema, 'users');

module.exports = User;
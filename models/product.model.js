var mongoees = require('mongoose');

var productSchema = new mongoees.Schema({
    name: String,
    image: String,
    description: String
});

var Product = mongoees.model('Product', productSchema, 'products');

module.exports = Product;